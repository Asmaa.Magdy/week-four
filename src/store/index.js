import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        dataN: {
            title: [],
            description: [],
            compelete: [],
        },
    },
    getters: {
        getTitle(state) {
            return state.dataN.title;
        },
        getDescription(state) {
            return state.dataN.description;
        },
        getcCmpelete(state) {
            return state.dataN.compelete;
        },
    },
    mutations: {
        setCompelete(state, {i,compelete}) {
          
            state.dataN.compelete = localStorage.compelete.split(',');
            state.dataN.compelete.splice(i, 1, compelete);
            console.log("dddd");
            console.log(localStorage.compelete.split(','));
            console.log(state.dataN.compelete);
            localStorage.removeItem('compelete');
            localStorage.setItem('compelete', state.dataN.compelete);
            console.log(state.dataN.compelete);
            console.log("dddd");
           
            console.log(i,i);
        },
        saveTitle(state, titleData) {
            if (localStorage.titles) {
                console.log('k');
                console.log(titleData.title);
                state.dataN.title.push(localStorage.getItem('titles'));
                state.dataN.description.push(
                    localStorage.getItem('descriptions')
                );
                state.dataN.compelete.push(localStorage.getItem('compelete'));
                state.dataN.compelete.push(titleData.compelete);
                console.log(JSON.stringify(titleData));
                state.dataN.title.push(titleData.title);
                state.dataN.description.push(titleData.description);
                console.log(state.dataN.title);
                localStorage.removeItem('titles');
                localStorage.removeItem('descriptions');
                localStorage.removeItem('compelete');
                localStorage.setItem('titles', state.dataN.title);
                localStorage.setItem('compelete', state.dataN.compelete);
                localStorage.setItem('descriptions', state.dataN.description);
                console.log(state.dataN);
            } else {
                console.log('kk');
                state.dataN.title.push(titleData.title);
                state.dataN.description.push(titleData.description);
                state.dataN.compelete.push(titleData.compelete);
                console.log(state.dataN.title);
                localStorage.setItem('compelete', state.dataN.compelete);
                localStorage.setItem('titles', state.dataN.title);
                localStorage.setItem('descriptions', state.dataN.description);
            }
        },

        editDescriptione(state, { i, dilogTitletitle, dilogDescription }) {
            console.log(dilogDescription, dilogTitletitle);
            console.log(i);
            console.log(localStorage.titles);
            state.dataN.title = localStorage.titles.split(',');
            console.log(state.dataN.title[i]);
            state.dataN.title.splice(i, 1, dilogTitletitle);
            console.log(state.dataN.title);
            console.log(localStorage.descriptions);
            state.dataN.description = localStorage.descriptions.split(',');
            console.log(state.dataN.description[i]);
            state.dataN.description.splice(i, 1, dilogDescription);
            console.log(state.dataN.description);

            localStorage.removeItem('titles');
            console.log(localStorage.getItem('titles'));
            localStorage.setItem('titles', state.dataN.title);
            localStorage.removeItem('descriptions');
            localStorage.setItem('descriptions', state.dataN.description);

            console.log(localStorage.titles);
        },
        deleteNote(state, i) {
            console.log(i);
            state.dataN.title = localStorage.titles.split(',');
            console.log(state.dataN.title);
            state.dataN.title.splice(i, 1);
            console.log(state.dataN.title);
            console.log(':');
            state.dataN.description = localStorage.descriptions.split(',');
            state.dataN.description.splice(i, 1);

            localStorage.removeItem('titles');
            console.log(localStorage.getItem('titles'));
            localStorage.removeItem('descriptions');
            localStorage.setItem('titles', state.dataN.title);

            localStorage.setItem('descriptions', state.dataN.description);

            console.log(localStorage.getItem('titles'));
        },
    },
    actions: {
        saveData({ commit }) {
            commit('saveData');
            console.log('act');
        },

        deleteData({ commit, state }, i) {
            commit('deleteNote', i);
            console.log(state);
        },
    },
    modules: {},
});
