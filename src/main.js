import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

Vue.filter('short', function (text) {
    if (text.length >= 15) {
        return text.substring(0, 20) + '...';
    } else {
        return text;
    }
});

export const eventBus = new Vue();


new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
   
}).$mount('#app');
